/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csb.gui;

import csb.CSB_PropertyType;
import csb.data.Course;
import csb.data.Assignment;
import static csb.gui.CSB_GUI.CLASS_HEADING_LABEL;
import static csb.gui.CSB_GUI.CLASS_PROMPT_LABEL;
import static csb.gui.CSB_GUI.PRIMARY_STYLE_SHEET;
import java.time.LocalDate;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 *
 * @author Kenneth
 */
public class AssignmentItemDialog extends Stage{
    //main item
    Assignment assignmentItem;
    
    //gui controls for dialog
    GridPane gridPane;
    Scene dialogScene;
    Label headingLabel;
    Label nameLabel;
    TextField nameTextField;
    Label topicsLabel;
    TextField topicsTextField;
    Label dateLabel;
    DatePicker datePicker;
    Button completeButton;
    Button cancelButton;
    
    
    String selection;
    
    public static final String COMPLETE = "Complete";
    public static final String CANCEL = "Cancel";
    public static final String NAME_PROMPT = "Name: ";
    public static final String TOPICS_PROMPT = "Topics: ";
    public static final String DUE_DATE_PROMPT = "Due Date: ";
    public static final String ASSIGNMENT_ITEM_HEADING = "Assignment Item Details";
    public static final String ADD_ASSIGNMENT_ITEM_TITLE = "Add New Assignment Item";
    public static final String EDIT_ASSIGNMENT_ITEM_TITLE = "Edit Assignment Item";
    
    public AssignmentItemDialog(Stage primaryStage, Course course, MessageDialog messageDialog){
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        headingLabel = new Label(ASSIGNMENT_ITEM_HEADING);
        headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);
        
        //first name 
        nameLabel = new Label(NAME_PROMPT);
        nameLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        nameTextField = new TextField();
        nameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            assignmentItem.setName(newValue);
        });
        
        // then topics
        topicsLabel = new Label(TOPICS_PROMPT);
        topicsLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        topicsTextField = new TextField();
        topicsTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            assignmentItem.setTopics(newValue);
        });
        
        //then the date
        dateLabel = new Label(DUE_DATE_PROMPT);
        dateLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        datePicker = new DatePicker();
        datePicker.setOnAction(e -> {
            if (datePicker.getValue().isBefore(course.getStartingMonday())
                    || datePicker.getValue().isAfter(course.getEndingFriday())) {
                // INCORRECT SELECTION, NOTIFY THE USER
                PropertiesManager props = PropertiesManager.getPropertiesManager();
                messageDialog.show(props.getProperty(CSB_PropertyType.ILLEGAL_DATE_MESSAGE));
            }             
            else {
                assignmentItem.setDate(datePicker.getValue());
            }
        });
        
        
        //buttons
        completeButton = new Button(COMPLETE);
        cancelButton = new Button(CANCEL);
        EventHandler completeCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            AssignmentItemDialog.this.selection = sourceButton.getText();
            AssignmentItemDialog.this.hide();
        };
        completeButton.setOnAction(completeCancelHandler);
        cancelButton.setOnAction(completeCancelHandler);
        //arrangements
        
        gridPane.add(headingLabel, 0, 0, 2, 1);
        gridPane.add(nameLabel, 0, 1, 1, 1);
        gridPane.add(nameTextField, 1, 1, 1, 1);
        gridPane.add(topicsLabel, 0, 2, 1, 1);
        gridPane.add(topicsTextField, 1, 2, 1, 1);
        gridPane.add(dateLabel, 0, 3, 1, 1);
        gridPane.add(datePicker, 1, 3, 1, 1);
        gridPane.add(completeButton, 0, 4, 1, 1);
        gridPane.add(cancelButton, 1, 4, 1, 1);
        
        dialogScene = new Scene(gridPane);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);
    }
    
    public String getSelection(){
        return selection;
    }
    
    public Assignment getAssignmentItem(){
        return assignmentItem;
    }
    
    public Assignment showAddAssignmentItemDialog(LocalDate initDate){
        setTitle(ADD_ASSIGNMENT_ITEM_TITLE);
        assignmentItem = new Assignment();
        
        nameTextField.setText(assignmentItem.getName());
        topicsTextField.setText(assignmentItem.getTopics());
        datePicker.setValue(initDate);
        
        this.showAndWait();
        
        return assignmentItem;
    }
    
    public void loadGUIData(){
        nameTextField.setText(assignmentItem.getName());
        topicsTextField.setText(assignmentItem.getTopics());
        datePicker.setValue(assignmentItem.getDate());
    }
    
    public boolean wasCompleteSelected() {
        return selection.equals(COMPLETE);
    }
    
    public void showEditAssignmentItemDialog(Assignment itemToEdit){
        
        setTitle(EDIT_ASSIGNMENT_ITEM_TITLE);
        
        assignmentItem = new Assignment();
        assignmentItem.setName(itemToEdit.getName());
        assignmentItem.setTopics(itemToEdit.getTopics());
        assignmentItem.setDate(itemToEdit.getDate());
        
        loadGUIData();
        this.showAndWait();
    }
}
