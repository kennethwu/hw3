/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csb.gui;

import csb.CSB_PropertyType;
import csb.data.Course;
import csb.data.Lecture;
import static csb.gui.CSB_GUI.CLASS_HEADING_LABEL;
import static csb.gui.CSB_GUI.CLASS_PROMPT_LABEL;
import static csb.gui.CSB_GUI.PRIMARY_STYLE_SHEET;
import java.awt.event.ActionListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 *
 * @author Kenneth
 */
public class LectureItemDialog extends Stage{
    
    Lecture lectureItem;
    
    GridPane gridPane;
    Scene dialogScene;
    Label headingLabel;
    Label topicLabel;
    TextField topicTextField;
    Label sessionLabel;
    ComboBox sessionComboBox;
    Button completeButton;
    Button cancelButton;
    
    String selection;
    
    public static final String COMPLETE = "Complete";
    public static final String CANCEL = "Cancel";
    public static final String TOPIC_PROMPT = "Topic: ";
    public static final String SESSION_PROMPT = "Number Of Sessions: ";
    public static final String LECTURE_ITEM_HEADING = "Lecture Details";
    public static final String ADD_LECTURE_ITEM_TITLE = "Add New Lecture Item";
    public static final String EDIT_LECTURE_ITEM_TITLE = "Edit Lecture Item";
    
    public LectureItemDialog(Stage primaryStage, Course course, MessageDialog messageDialog){
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        headingLabel = new Label(LECTURE_ITEM_HEADING);
        headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);
        
        //TOPIC
        topicLabel = new Label(TOPIC_PROMPT);
        topicLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        topicTextField = new TextField();
        topicTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            lectureItem.setTopic(newValue);
        });
        //Sessions
        sessionLabel = new Label(SESSION_PROMPT);
        sessionLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        sessionComboBox = new ComboBox();
        sessionComboBox.getItems().add(new Integer(1));
        sessionComboBox.getItems().add(new Integer(2));
        sessionComboBox.getItems().add(new Integer(3));
        sessionComboBox.getItems().add(new Integer(4));
        sessionComboBox.getItems().add(new Integer(5));
        sessionComboBox.getItems().add(new Integer(6));
        sessionComboBox.getItems().add(new Integer(7));
        sessionComboBox.getItems().add(new Integer(8));
        sessionComboBox.getItems().add(new Integer(9));
        
        sessionComboBox.setOnAction(e -> {
            
            try{
            int a =(int)sessionComboBox.getSelectionModel().getSelectedItem();
            lectureItem.setSessions(a);
                    } catch (NullPointerException ex){
                
            }
            
          //  a = a + 10;
          //  System.out.println(a);
            
        });
        
        
        completeButton = new Button(COMPLETE);
        cancelButton = new Button(CANCEL);
        EventHandler completeCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            LectureItemDialog.this.selection = sourceButton.getText();
            LectureItemDialog.this.hide();
        };
        completeButton.setOnAction(completeCancelHandler);
        cancelButton.setOnAction(completeCancelHandler);
        
        //arrangement
        gridPane.add(headingLabel, 0, 0, 2, 1);
        gridPane.add(topicLabel, 0, 1, 1, 1);
        gridPane.add(topicTextField, 1, 1, 1, 1);
        gridPane.add(sessionLabel, 0, 2, 1, 1);
        gridPane.add(sessionComboBox, 1, 2, 1, 1);
        gridPane.add(completeButton, 0, 4, 1, 1);
        gridPane.add(cancelButton, 1, 4, 1, 1);
        dialogScene = new Scene(gridPane);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);
        
    }
    
    
    //accessors
    public String getSelection() {
        return selection;
    }      
    
    public Lecture getLectureItem() { 
        return lectureItem;
    }
    
    public Lecture showAddLectureItemDialog() {
        // SET THE DIALOG TITLE
        setTitle(ADD_LECTURE_ITEM_TITLE);
        
        // RESET THE lecture ITEM OBJECT WITH DEFAULT VALUES
        lectureItem = new Lecture();
        
        // LOAD THE UI STUFF
        topicTextField.setText(lectureItem.getTopic());
        sessionComboBox.setValue(lectureItem.getSessions());
        
        // AND OPEN IT UP
        this.showAndWait();
        
        return lectureItem;
    }
    
    public void loadGUIData() {
        // LOAD THE UI STUFF
        topicTextField.setText(lectureItem.getTopic());
        sessionComboBox.setValue(lectureItem.getSessions());       
    }
    
     public boolean wasCompleteSelected() {
        return selection.equals(COMPLETE);
    }
     
     public void showEditLectureItemDialog(Lecture itemToEdit) {
        // SET THE DIALOG TITLE
        setTitle(EDIT_LECTURE_ITEM_TITLE);
        
        // LOAD THE SCHEDULE ITEM INTO OUR LOCAL OBJECT
        lectureItem = new Lecture();
        lectureItem.setTopic(itemToEdit.getTopic());
        lectureItem.setSessions(itemToEdit.getSessions());
        
        // AND THEN INTO OUR GUI
        loadGUIData();
               
        // AND OPEN IT UP
        this.showAndWait();
    }
    
}
