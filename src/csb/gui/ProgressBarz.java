package csb.gui;



import java.util.ArrayList;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 *
 * @author McKillaGorilla
 */
public class ProgressBarz extends Application {
    ProgressBar bar;
    ProgressIndicator indicator;
    Button button;
    Label processLabel;
    int numTasks = 0;
    
    ArrayList<String> lol = new ArrayList<String>();
    int totalz = 0;
    Stage wb;
    
    public void load(int totalzm, ArrayList<String> items, Stage wb){
        this.totalz = totalzm;
        lol = items;
        //System.out.println(totalz);
        this.wb = wb;
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        VBox box = new VBox();
        

        HBox toolbar = new HBox();
        bar = new ProgressBar(0);      
        indicator = new ProgressIndicator(0);
        indicator.setStyle("font-size: 14pt");
        
        toolbar.getChildren().add(bar);
        toolbar.getChildren().add(indicator);
        toolbar.setAlignment(Pos.CENTER);
        
        processLabel = new Label();
        processLabel.setFont(new Font("Serif", 20));
        processLabel.setAlignment(Pos.CENTER);
       
        box.getChildren().add(processLabel);
        box.setAlignment(Pos.CENTER);
        box.getChildren().add(toolbar);
        
        
        
        
        Scene scene = new Scene(box);
        primaryStage.setScene(scene);
        primaryStage.setHeight(200);
        primaryStage.setWidth(400);
        

        if(totalz == 4)    {
           // System.out.println("huh");
                Task<Void> task = new Task<Void>() {
                    int task = numTasks++;
                    double max = 1000;
                    double perc;
                    String catz = "LOADING";
                    @Override
                    protected Void call() throws Exception {
                        for (int i = 1; i <= 1000; i++) {
                            //System.out.println(i);
                            perc = i/max;
                            
                            // THIS WILL BE DONE ASYNCHRONOUSLY VIA MULTITHREADING
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    
                                    
                                    
                                    
                                    
                                    if(perc == 0.00){
                                        processLabel.setText("Loading");
                                        bar.setProgress(perc);
                                        indicator.setProgress(perc);
                                    }
                                    
                                    
                                    if(perc == 0.25){
                                        processLabel.setText("Exporting " + lol.get(0) +" Completed");
                                        bar.setProgress(perc);
                                         indicator.setProgress(perc);
                                
                                   
                                    try { 
                                        Thread.sleep(100);
                                   }catch (InterruptedException ie) {
                                        ie.printStackTrace();
                                   }
                            }
                                    if(perc == 0.5){
                                        processLabel.setText("Exporting " + lol.get(1) +" Completed");
                                        bar.setProgress(perc);
                                    indicator.setProgress(perc);
                                
                                   
                                    try { 
                                        Thread.sleep(100);
                                   }catch (InterruptedException ie) {
                                        ie.printStackTrace();
                                   }
                            }
                                    if(perc == 0.75){
                                        processLabel.setText("Exporting " + lol.get(2) +" Completed");
                                        bar.setProgress(perc);
                                    indicator.setProgress(perc);
                                
                                   
                                    try { 
                                        Thread.sleep(100);
                                   }catch (InterruptedException ie) {
                                        ie.printStackTrace();
                                   }
                            }
                                    
                                    if(perc == 1){
                                        processLabel.setText("Exporting " + lol.get(3) +" Completed");
                                        bar.setProgress(perc);
                                    indicator.setProgress(perc);
                                
                                   
                                    try { 
                                        Thread.sleep(100);
                                   }catch (InterruptedException ie) {
                                        ie.printStackTrace();
                                   }
                                    wb.show();
                                    //primaryStage.hide();
                            }
                                    
                                }
                            });
                            
                            
                            

                            // SLEEP EACH FRAME
                            try {
                                Thread.sleep(10);
                            } catch (InterruptedException ie) {
                                ie.printStackTrace();
                            }
                        }
                        return null;
                    }
                };
                // THIS GETS THE THREAD ROLLING
                Thread thread = new Thread(task);
                thread.start();            
            
        primaryStage.show();
    }
        
        if(totalz == 5)    {
           // System.out.println("huh");
                Task<Void> task = new Task<Void>() {
                    int task = numTasks++;
                    double max = 1000;
                    double perc;
                    String catz = "LOADING";
                    @Override
                    protected Void call() throws Exception {
                        for (int i = 1; i <= 1000; i++) {
                            //System.out.println(i);
                            perc = i/max;
                            
                            // THIS WILL BE DONE ASYNCHRONOUSLY VIA MULTITHREADING
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    
                                    
                                    
                                    
                                    
                                    if(perc == 0.00){
                                        processLabel.setText("START");
                                        bar.setProgress(perc);
                                        indicator.setProgress(perc);
                                    }
                                    
                                    
                                    if(perc == 0.20){
                                        processLabel.setText("Exporting " + lol.get(0) +" Completed");
                                        bar.setProgress(perc);
                                         indicator.setProgress(perc);
                                
                                   
                                    try { 
                                        Thread.sleep(100);
                                   }catch (InterruptedException ie) {
                                        ie.printStackTrace();
                                   }
                            }
                                    if(perc == 0.40){
                                        processLabel.setText("Exporting " + lol.get(1) +" Completed");
                                        bar.setProgress(perc);
                                    indicator.setProgress(perc);
                                
                                   
                                    try { 
                                        Thread.sleep(100);
                                   }catch (InterruptedException ie) {
                                        ie.printStackTrace();
                                   }
                            }
                                    if(perc == 0.60){
                                        processLabel.setText("Exporting " + lol.get(2) +" Completed");
                                        bar.setProgress(perc);
                                    indicator.setProgress(perc);
                                
                                   
                                    try { 
                                        Thread.sleep(100);
                                   }catch (InterruptedException ie) {
                                        ie.printStackTrace();
                                   }
                            }
                                    if(perc == 0.80){
                                        processLabel.setText("Exporting " + lol.get(3) +" Completed");
                                        bar.setProgress(perc);
                                    indicator.setProgress(perc);
                                
                                   
                                    try { 
                                        Thread.sleep(100);
                                   }catch (InterruptedException ie) {
                                        ie.printStackTrace();
                                   }
                            }
                                    
                                    if(perc == 1.00){
                                        processLabel.setText("Exporting " + lol.get(4) +" Completed");
                                        bar.setProgress(perc);
                                    indicator.setProgress(perc);
                                
                                   
                                    try { 
                                        Thread.sleep(100);
                                   }catch (InterruptedException ie) {
                                        ie.printStackTrace();
                                   }
                                    wb.show();
                                    //primaryStage.hide();
                            }
                                    
                                }
                            });
                            
                            
                           

                            // SLEEP EACH FRAME
                            try {
                                Thread.sleep(10);
                            } catch (InterruptedException ie) {
                                ie.printStackTrace();
                            }
                        }
                        return null;
                    }
                };
                // THIS GETS THE THREAD ROLLING
                Thread thread = new Thread(task);
                thread.start();            
            
        primaryStage.show();
    }
        
        if(totalz == 3)    {
           // System.out.println("huh");
                Task<Void> task = new Task<Void>() {
                    int task = numTasks++;
                    double max = 500;
                    double perc;
                    String catz = "LOADING";
                    @Override
                    protected Void call() throws Exception {
                        for (int i = 1; i <= 500; i++) {
                            //System.out.println(i);
                            perc = i/max;
                            
                            // THIS WILL BE DONE ASYNCHRONOUSLY VIA MULTITHREADING
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    
                                    
                                    
                                    
                                    
                                    if(perc == 0.00){
                                        processLabel.setText("Loading");
                                        bar.setProgress(perc);
                                        indicator.setProgress(perc);
                                    }
                                    
                                    
                                    if(perc == 0.33){
                                        processLabel.setText("Exporting " + lol.get(0) +" Completed");
                                        bar.setProgress(perc);
                                         indicator.setProgress(perc);
                                
                                   
                                    try { 
                                        Thread.sleep(100);
                                   }catch (InterruptedException ie) {
                                        ie.printStackTrace();
                                   }
                            }
                                    if(perc == 0.67){
                                        processLabel.setText("Exporting " + lol.get(1) +" Completed");
                                        bar.setProgress(perc);
                                    indicator.setProgress(perc);
                                
                                   
                                    try { 
                                        Thread.sleep(100);
                                   }catch (InterruptedException ie) {
                                        ie.printStackTrace();
                                   }
                            }
                                    if(perc == 1.00){
                                        processLabel.setText("Exporting " + lol.get(2) +" Completed");
                                        bar.setProgress(perc);
                                    indicator.setProgress(perc);
                                
                                   
                                    try { 
                                        Thread.sleep(100);
                                   }catch (InterruptedException ie) {
                                        ie.printStackTrace();
                                   }
                                    wb.show();
                            }
                                    
                                    
                                    
                                }
                            });
                            
                            
                            

                            // SLEEP EACH FRAME
                            try {
                                Thread.sleep(10);
                            } catch (InterruptedException ie) {
                                ie.printStackTrace();
                            }
                        }
                        return null;
                    }
                };
                // THIS GETS THE THREAD ROLLING
                Thread thread = new Thread(task);
                thread.start();            
            
        primaryStage.show();
    }
        if(totalz == 2)    {
           // System.out.println("huh");
                Task<Void> task = new Task<Void>() {
                    int task = numTasks++;
                    double max = 300;
                    double perc;
                    String catz = "LOADING";
                    @Override
                    protected Void call() throws Exception {
                        for (int i = 1; i <= 300; i++) {
                            //System.out.println(i);
                            perc = i/max;
                            
                            // THIS WILL BE DONE ASYNCHRONOUSLY VIA MULTITHREADING
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    
                                    
                                    
                                    
                                    
                                    if(perc == 0.00){
                                        processLabel.setText("Loading");
                                        bar.setProgress(perc);
                                        indicator.setProgress(perc);
                                    }
                                    
                                    
                                    if(perc == 0.50){
                                        processLabel.setText("Exporting " + lol.get(0) +" Completed");
                                        bar.setProgress(perc);
                                         indicator.setProgress(perc);
                                
                                   
                                    try { 
                                        Thread.sleep(100);
                                   }catch (InterruptedException ie) {
                                        ie.printStackTrace();
                                   }
                            }
                                    if(perc == 1.00){
                                        processLabel.setText("Exporting " + lol.get(1) +" Completed");
                                        bar.setProgress(perc);
                                    indicator.setProgress(perc);
                                
                                   
                                    try { 
                                        Thread.sleep(100);
                                   }catch (InterruptedException ie) {
                                        ie.printStackTrace();
                                   }
                                    wb.show();
                            }
                                    
                                    
                                    
                                    
                                }
                            });
                            
                            
                            

                            // SLEEP EACH FRAME
                            try {
                                Thread.sleep(10);
                            } catch (InterruptedException ie) {
                                ie.printStackTrace();
                            }
                        }
                        return null;
                    }
                };
                // THIS GETS THE THREAD ROLLING
                Thread thread = new Thread(task);
                thread.start();            
            
        primaryStage.show();
    }
        if(totalz == 1)    {
           // System.out.println("huh");
                Task<Void> task = new Task<Void>() {
                    int task = numTasks++;
                    double max = 150;
                    double perc;
                    String catz = "LOADING";
                    @Override
                    protected Void call() throws Exception {
                        for (int i = 1; i <= 150; i++) {
                            //System.out.println(i);
                            perc = i/max;
                            
                            // THIS WILL BE DONE ASYNCHRONOUSLY VIA MULTITHREADING
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    
                                    
                                    
                                    
                                    
                                    if(perc == 0.00){
                                        processLabel.setText("Loading");
                                        bar.setProgress(perc);
                                        indicator.setProgress(perc);
                                    }
                                    
                                    
                                    if(perc == 1.00){
                                        processLabel.setText("Exporting " + lol.get(0) +" Completed");
                                        bar.setProgress(perc);
                                         indicator.setProgress(perc);
                                
                                   
                                    try { 
                                        Thread.sleep(100);
                                   }catch (InterruptedException ie) {
                                        ie.printStackTrace();
                                   }
                                    wb.show();
                            }
                                  
                                    
                                    
                                    
                                    
                                }
                            });
                            
                            
                            

                            // SLEEP EACH FRAME
                            try {
                                Thread.sleep(10);
                            } catch (InterruptedException ie) {
                                ie.printStackTrace();
                            }
                        }
                        return null;
                    }
                };
                // THIS GETS THE THREAD ROLLING
                Thread thread = new Thread(task);
                thread.start();            
            
        primaryStage.show();
    }
    
    }
    public static void main(String[] args) {
        launch(args);
    }
}